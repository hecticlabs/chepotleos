#include "source.h"
#include "config.h"

char SERIAL_BOOTCTL[SERIAL_BOOTCTL_SIZE];
pico_unique_board_id_t ubid;

union intconv intload;
unsigned char *loadbuf = NULL;

void print(char *s) {
  printf("%s", s);
  putchar(0);
}

void process_cmd(char *cmd) {
  if (strcmp(cmd, "board") == 0) {
    printf("ChepotleOS %s %s (%s)\n", VERSION, BUILDHASH, COMPILETIME);
    printf("UBID: ");
    for (int i = 0; i < PICO_UNIQUE_BOARD_ID_SIZE_BYTES; ++i) {
      printf("%02x", ubid.id[i]);
    }
    printf("\n");
    putchar(0);
  } else if (strcmp(cmd, "flash") == 0) {
#ifdef CHEPOTLE_FLASHING
    printf("[accepting tar file]\n");
    // TODO read tar file into the filesystem
#else
    printf("flashing is disabled!\n");
#endif
  } else if (strcmp(cmd, "reboot") == 0) {
    watchdog_enable(1, 1);
    while(1);
#ifdef CHEPOTLE_FIRMWARE_FLASHING
  } else if (strcmp(cmd, "reboot flashing") == 0) {
    reset_usb_boot(0, 0);
#endif
  } else if (strcmp(cmd, "help") == 0) {
    printf("board\n");
    printf("help\n");
    printf("flash\n");
    printf("reboot\n");
#ifdef CHEPOTLE_FIRMWARE_FLASHING
    printf("reboot flashing\n");
    printf("load\n");
    printf("jump\n");
    printf("boot\n");
    printf("hexdump\n");
#endif
    putchar(0);
  } else if (strcmp(cmd, "load") == 0) {
    printf("[accepting binary]\n");
    putchar(0);
    int b = read(intload.b, 4);
    if (b != 4) {
      printf("error: invalid length\n");
      putchar(0);
      return;
    }
    if (intload.x > 96 * 1024) {
      printf("error: buffer over 96 KiB\n");
      putchar(0);
      return;
    }
    if (loadbuf != NULL)
      free(loadbuf);
    loadbuf = malloc(intload.x);
    b = read(loadbuf, intload.x);
    printf("loaded %d bytes into buffer at %p\n", b, loadbuf);
    putchar(0);
  } else if (strcmp(cmd, "jump") == 0) {
    printf("jumping into function at %p\n", loadbuf);
    putchar(0);
    //int (*jump)(char*) = (int (*)(char*)) (loadbuf+1);
    //int n = jump("-v");
    int (*jump)() = (int (*)(void (*)(char*))) (loadbuf+1);
    int n = jump(print);
    printf("exited with code %d\n", n);
    putchar(0);
  } else if (strcmp(cmd, "boot") == 0) {
    printf("booting ELF kernel at %p\n", loadbuf);
    ElfHeader *header = (ElfHeader*) loadbuf;

    // Verify magic
    if (header->e_ident[0] != 0x7F || header->e_ident[1] != 'E' || header->e_ident[2] != 'L' || header->e_ident[3] != 'F') {
      printf("error: invalid magic\n");
      putchar(0);
      return;
    }
    if (header->e_machine != 40) {
      printf("error: not arm32 thumb\n");
      putchar(0);
      return;
    }

    KernelEntryPoint entry = load_elf(header);

    printf("entrypoint at %p\n", entry);
    putchar(0);

    int r = entry("-v");
    printf("kernel returned %d\n", r);
    putchar(0);
  } else if (strncmp(cmd, "hexdump", 7) == 0) {
    int a, n;
    sscanf(cmd, "%*s %x %d", &a, &n);
    DumpHex((void*) a, n);
    putchar(0);
  } else {
    printf("error unknown command: %s", cmd);
    putchar(0);
  }
}

int main() {
  stdio_init_all();
  pico_get_unique_board_id(&ubid);

  printf("checking for data volume...\n");
  // TODO check for data volume and boot it

  printf("data volume not found\nentering bootctl mode...\n");
#ifdef CHEPOTLE_ENFORCE_DATA
  printf("enforce data detected, bootctl disabled\n");
  return 1;
#endif

  while (1) {
    memset(SERIAL_BOOTCTL, 0, SERIAL_BOOTCTL_SIZE);
    int c = sgets(SERIAL_BOOTCTL, SERIAL_BOOTCTL_SIZE);
    if (c >= 0) {
      process_cmd(SERIAL_BOOTCTL);
    }
    sleep_ms(100);
  }
}

