#include "source.h"

int sgets(char *str, int l) {
  int i = 0;
  while ((*str = getchar()) != 0 && l-- > 0) {
    if (*str == EOF) break;
    str++;
    i++;
  }
  *str = '\0';
  return i;
}

void DumpHex(const void* data, size_t size) {
  char ascii[17];
  size_t i, j;
  ascii[16] = '\0';
  for (i = 0; i < size; ++i) {
    printf("%02X ", ((unsigned char*)data)[i]);
    if (((unsigned char*)data)[i] >= ' ' && ((unsigned char*)data)[i] <= '~') {
      ascii[i % 16] = ((unsigned char*)data)[i];
    } else {
      ascii[i % 16] = '.';
    }
    if ((i+1) % 8 == 0 || i+1 == size) {
      printf(" ");
      if ((i+1) % 16 == 0) {
        printf("|  %s \n", ascii);
      } else if (i+1 == size) {
        ascii[(i+1) % 16] = '\0';
        if ((i+1) % 16 <= 8) {
          printf(" ");
        }
        for (j = (i+1) % 16; j < 16; ++j) {
          printf("   ");
        }
        printf("|  %s \n", ascii);
      }
    }
  }
}

int read(char *buffer, int n) {
  int i;
  for (i = 0; i < n; i++) {
    int c = getchar();

    if (c == EOF) {
      break;
    }

    buffer[i] = c;
  }

  return i;
}
