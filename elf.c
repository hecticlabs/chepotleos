#include "source.h"

KernelEntryPoint load_elf(ElfHeader *elf_header) {
  uintptr_t base_address = (uintptr_t) elf_header;

  ElfProgramHeader *program_header = (ElfProgramHeader*)(base_address + elf_header->e_phoff);
  for (int i = 0; i < elf_header->e_phnum; ++i) {
    if (program_header[i].p_type == PT_LOAD) {
      // Calculate the virtual address for the current section
      uintptr_t virtual_address = base_address + program_header[i].p_vaddr;

      // Calculate the offset of the current section in the ELF file
      uintptr_t file_offset = base_address + program_header[i].p_offset;

      // Copy the section data from the ELF file to its new location in SRAM
      memcpy((void *)virtual_address, (const void *)file_offset, program_header[i].p_filesz);

      // Zero out any remaining bytes (if necessary)
      uintptr_t remaining_bytes = program_header[i].p_memsz - program_header[i].p_filesz;
      if (remaining_bytes > 0) {
        memset((void *)(virtual_address + program_header[i].p_filesz), 0, remaining_bytes);
      }
    }
  }

  return (KernelEntryPoint)(base_address + elf_header->e_entry);
}
