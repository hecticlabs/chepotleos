#ifndef _CONFIG_H
#define _CONFIG_H

#define VERSION "v1.0"
#define COMPILETIME __DATE__ " " __TIME__

#define CHEPOTLE_FLASHING
#define CHEPOTLE_FIRMWARE_FLASHING
//#define CHEPOTLE_ENFORCE_DATA

#endif

#ifndef BUILDHASH
#define BUILDHASH "invalid"
#endif
