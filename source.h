#ifndef _SOURCE_H
#define _SOURCE_H

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "pico/stdlib.h"
#include "pico/unique_id.h"
#include "pico/bootrom.h"
#include "hardware/watchdog.h"

#define SERIAL_BOOTCTL_SIZE 256

typedef struct __attribute__((__packed__)) {
  uint8_t e_ident[16];
  uint16_t e_type;
  uint16_t e_machine;
  uint32_t e_version;
  uint32_t e_entry;
  uint32_t e_phoff;
  uint32_t e_shoff;
  uint32_t e_flags;
  uint16_t e_ehsize;
  uint16_t e_phentsize;
  uint16_t e_phnum;
  uint16_t e_shentsize;
  uint16_t e_shnum;
  uint16_t e_shstrndx;
} ElfHeader;

typedef struct {
    uint32_t p_type;
    uint32_t p_offset;
    uint32_t p_vaddr;
    uint32_t p_paddr;
    uint32_t p_filesz;
    uint32_t p_memsz;
    uint32_t p_flags;
    uint32_t p_align;
} ElfProgramHeader;

#define PT_LOAD 1

typedef int (*KernelEntryPoint)(char *cmdline);

int sgets(char *str, int l);
void DumpHex(const void* data, size_t size);
int read(char *buffer, int n);

KernelEntryPoint load_elf(ElfHeader *elf_header);

union intconv {
    uint32_t x;
    unsigned char b[4];
};

#endif
